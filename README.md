# MalaPrace

* [x] GIT repozitář na Gitlabu.
* [x] Repozitář má README.md
* [x] Použití libovolného generátoru statických stránek.
* [x] Vytvořená CI v repozitáři.
* [x] CI má minimálně dvě úlohy:
* [x] Test kvality Markdown stránek.
* [x] Generování HTML stránek z Markdown zdrojů.
* [x] CI má automatickou úlohou nasazení web stránek (deploy).
* [x] Gitlab projekt má fukční web stránky s generovaným obsahem
